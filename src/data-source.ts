import { DataSource } from 'typeorm';
import { CardsEntity } from './src/entities/cards.entity';
import { CardLabelsEntity } from './src/entities/card-labels.entity';
import { CardUsersEntity } from './src/entities/card-users.entity';
import { CommentsEntity } from './src/entities/comments.entity';
import { LabelsEntity } from './src/entities/labels.entity';
import { ListsEntity } from './src/entities/lists.entity';
import { UsersEntity } from './src/entities/users.entity';
import { WorkspaceUsersEntity } from './src/entities/workspace-users.entity';
import { WorkspacesEntity } from './src/entities/workspaces.entity';
import { database } from './src/config';
import {CreateFieldsAndRelationsWithUserAndComment1694176823330} from './src/migrations/1694176823330-create-fields-and-relations-with-user-and-comment';

  export const AppDataSource = new DataSource({
  type: 'postgres',
  host: database.host,
  port: database.port,
  username: database.user,
  password: database.password,
  database: database.database,
  synchronize: true,
  logging: false,
  entities: [
    CardsEntity,
    CardLabelsEntity,
    CardUsersEntity,
    CommentsEntity,
    LabelsEntity,
    ListsEntity,
    UsersEntity,
    WorkspaceUsersEntity,
    WorkspacesEntity,
  ],
  migrations: [CreateFieldsAndRelationsWithUserAndComment1694176823330]

});

