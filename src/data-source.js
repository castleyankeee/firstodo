
Object.defineProperty(exports, '__esModule', { value: true });
exports.AppDataSource = void 0;
var typeorm_1 = require('typeorm');
var cards_entity_1 = require('./src/entities/cards.entity');
var card_labels_entity_1 = require('./src/entities/card-labels.entity');
var card_users_entity_1 = require('./src/entities/card-users.entity');
var comments_entity_1 = require('./src/entities/comments.entity');
var labels_entity_1 = require('./src/entities/labels.entity');
var lists_entity_1 = require('./src/entities/lists.entity');
var users_entity_1 = require('./src/entities/users.entity');
var workspace_users_entity_1 = require('./src/entities/workspace-users.entity');
var workspaces_entity_1 = require('./src/entities/workspaces.entity');
var config_1 = require('./src/config');
exports.AppDataSource = new typeorm_1.DataSource({
    type: 'postgres',
    host: config_1.database.host,
    port: config_1.database.port,
    username: config_1.database.user,
    password: config_1.database.password,
    synchronize: true,
    logging: true,
    entities: [
        cards_entity_1.CardsEntity,
        card_labels_entity_1.CardLabelsEntity,
        card_users_entity_1.CardUsersEntity,
        comments_entity_1.CommentsEntity,
        labels_entity_1.LabelsEntity,
        lists_entity_1.ListsEntity,
        users_entity_1.UsersEntity,
        workspace_users_entity_1.WorkspaceUsersEntity,
        workspaces_entity_1.WorkspacesEntity,
    ],
});
