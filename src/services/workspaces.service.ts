import { AppDataSource } from '../../data-source';
import { WorkspacesDto } from '../dtos/workspaces.dto';
import { WorkspacesEntity } from '../entities/workspaces.entity';
import { WorkspaceUsersEntity } from '../entities/workspace-users.entity';

const workspaceRepository = AppDataSource.getRepository(WorkspacesEntity);
const workspaceUsersRepository = AppDataSource.getRepository(WorkspaceUsersEntity);


export class WorkspacesService {
  static async createWorkspace(userId: string, createWorkspaceDto: WorkspacesDto) {
    const newWorkspace = workspaceRepository.create(createWorkspaceDto);
    const workspace = await workspaceRepository.save(newWorkspace);
    const newWorkspaceUsers = workspaceUsersRepository.create({
      user_id: userId,
      workspace_id: workspace.id,
    });
    await workspaceUsersRepository.save(newWorkspaceUsers);
    return;
  }

  static async getAllData(id: string) {
    const workspace = await workspaceRepository.findOne({where: {id}});
    if (!workspace) {
      return null;
    } else {
      return await AppDataSource.transaction(async transactionalEntityManager => {
        return await workspaceRepository
            .createQueryBuilder('workspace')
            .leftJoinAndSelect('workspace.lists', 'lists')
            .leftJoinAndSelect('lists.cards', 'cards')
            .leftJoinAndSelect('cards.cardLabels', 'card_labels')
            .leftJoinAndSelect('card_labels.label', 'labels')
            .where('lists.workspace_id = :id', {id: workspace.id})
            .getMany();
      });
    }
  }
}