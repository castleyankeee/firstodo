import { AppDataSource } from '../../data-source';
import { LabelsEntity } from '../entities/labels.entity';
import { LabelsDTO } from '../dtos/labels.dto';
import { CardLabelsEntity } from '../entities/card-labels.entity';

const labelsRepository = AppDataSource.getRepository(LabelsEntity);
const cardLabelsRepository = AppDataSource.getRepository(CardLabelsEntity);

export class LabelsService {
  static async createLabels(createLabelsDto: LabelsDTO) {
    const newLabel = labelsRepository.create(createLabelsDto);
    await labelsRepository.save(newLabel);
    return;
  }

  static async assignLabel(labelId: string, cardId: string) {
    const currentLabels = await cardLabelsRepository.find({
      where: {card_id: cardId},
      order: {order: 'ASC'},
    });
      let newLabelOrder = 1;
      if (currentLabels.length > 0) {
        const lastLabel = currentLabels[currentLabels.length - 1];
        newLabelOrder = lastLabel.order + 1;
      }
      const newAssign = cardLabelsRepository.create({
        label_id: labelId,
        card_id: cardId,
        order: newLabelOrder,
      });
      return await cardLabelsRepository.save(newAssign);
    }
  }