import {CardsDto} from '../dtos/cards.dto';
import {AppDataSource} from '../../data-source';
import {CardsEntity} from '../entities/cards.entity';
import {CardUsersEntity} from '../entities/card-users.entity';

const cardsRepository = AppDataSource.getRepository(CardsEntity);
const cardUsersRepository = AppDataSource.getRepository(CardUsersEntity);

export class CardsService {
  static async createCard(userId: string,createCardsDto: CardsDto) {
    const newCards = cardsRepository.create(createCardsDto);
    const card = await cardsRepository.save(newCards);
    const newCardUsers = cardUsersRepository.create({
      user_id: userId,
      card_id: card.id
    });
    await cardUsersRepository.save(newCardUsers);
    return;
  }

  static async getOneCard(id: string) {
    return await cardsRepository
      .createQueryBuilder('cards')
      .leftJoinAndSelect('cards.cardLabels', 'card_labels')
      .leftJoinAndSelect('card_labels.label', 'labels')
      .where('cards.id = :id', { id: id })
      .getOne();
  }

  static async changeList(cardId: string, newListId: string) {
    return await cardsRepository
      .createQueryBuilder()
      .update(CardsEntity)
      .set({ list_id: newListId })
      .where('cards.id = :id', { id: cardId })
      .execute();
  }
}
