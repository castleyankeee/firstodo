import { RegisterDTO } from '../dtos/register.dto';
import { AppDataSource } from '../../data-source';
import { UsersEntity } from '../entities/users.entity';
import { LoginDTO } from '../dtos/login.dto';

const jwt = require('jsonwebtoken');
const bcrypt = require('bcrypt');

const saltRound = 12;
const userRepository= AppDataSource.getRepository(UsersEntity);

export class AuthenticationService {
  static async register(registerUserDto: RegisterDTO) {
    const hashPassword = await bcrypt.hash(registerUserDto.password, saltRound);
    const newUser = userRepository.create({
      ...registerUserDto,
      password: hashPassword,
    });
    await userRepository.save(newUser);
    return;
  }

  static async login(loginUserDTO: LoginDTO) {
    const user = await userRepository.findOneBy({ email: loginUserDTO.email });
    if (!user) {
      return null;
    }
    const passwordIsValid = bcrypt.compareSync(loginUserDTO.password, user.password);
    if (passwordIsValid) {
      return jwt.sign({ id: user.id,email: user.email, role: user.role}, process.env.TOKEN_KEY, { expiresIn: '1h' });
    }
    return null;
  }
}




