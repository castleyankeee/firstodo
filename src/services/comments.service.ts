import {CommentsDTO} from '../dtos/comments.dto';
import {AppDataSource} from '../../data-source';
import {CommentsEntity} from '../entities/comments.entity';

const commentsRepository = AppDataSource.getRepository(CommentsEntity);

export class CommentsService {
    static async createComment(createCommentsDto: CommentsDTO) {
        if (createCommentsDto.parent_id) {
            const newReply = commentsRepository.create({
                ...createCommentsDto,
                reply_user_id: createCommentsDto.user_id
            });
            return await commentsRepository.save(newReply);
        } else {
            const newComment = commentsRepository.create(createCommentsDto);
            return await commentsRepository.save(newComment);
        }
    }

    static async getAllComments(page: number, limit: number) {
        const offset = (page - 1) * limit;

        return await commentsRepository.find({
            skip: offset,
            take: limit,
        });
    }

    static async editComments(commentId: string, content: string) {
        return await commentsRepository
            .createQueryBuilder()
            .update(CommentsEntity)
            .set({content: content})
            .where('comments.id = :id', {id: commentId})
            .execute();
    }

    static async deleteComment(id: string) {
            const commentToDelete = await commentsRepository.findOne({
                where: {id}
            });
            if (commentToDelete) {
                await this.deleteRepliesCascade(commentToDelete);
                await commentsRepository.remove(commentToDelete);
                return;
            }
    }
    static async deleteRepliesCascade(parentComment: CommentsEntity) {
        const replies = await commentsRepository.find({
            where: { parent_id: parentComment.id },
        });
        if (replies.length > 0) {
            for (const reply of replies) {
                await this.deleteRepliesCascade(reply);
                await commentsRepository.remove(reply);
            }
        }
    }
}


