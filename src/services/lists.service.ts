import {ListsDTO} from '../dtos/lists.dto';
import {AppDataSource} from '../../data-source';
import {ListsEntity} from '../entities/lists.entity';


const listsRepository = AppDataSource.getRepository(ListsEntity);

export class ListsService {
  static async create(createListsDto: ListsDTO) {
    const newLists = listsRepository.create(createListsDto);

    await listsRepository.save(newLists);

    return;
  }
}
