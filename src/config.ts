import * as dotenv from 'dotenv';
import * as process from 'process';
dotenv.config();

const main = {
  port: process.env.PORT,
};
const database = {

  host: process.env.TD_HOST,
  port: 5432,
  database: process.env.TD_DATABASE,
  user: process.env.TD_USER,
  password: process.env.TD_PASSWORD,
};

export { main, database };
