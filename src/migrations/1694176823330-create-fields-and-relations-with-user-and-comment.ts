import { MigrationInterface, QueryRunner, TableColumn, TableForeignKey } from 'typeorm';

export class CreateFieldsAndRelationsWithUserAndComment1694176823330 implements MigrationInterface {
    async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.addColumn(
            'comments',
            new TableColumn({
                name: 'parent_id',
                type: 'uuid',
                isNullable: true,
            }),
        );

        await queryRunner.createForeignKey(

            'comments',
            new TableForeignKey({
                columnNames: ['parent_id'],
                referencedTableName: 'comments',
                referencedColumnNames: ['id'],
                onDelete: 'CASCADE',
                onUpdate: 'CASCADE',
            }),
        );

        await queryRunner.addColumn(
            'comments',
            new TableColumn({
                name: 'reply_user_id',
                type: 'uuid',
                isNullable: true,
            }),
        );

        await queryRunner.createForeignKey(
            'comments',
            new TableForeignKey({
                columnNames: ['reply_user_id'],
                referencedTableName: 'users',
                referencedColumnNames: ['id'],
                onDelete: 'SET NULL',
                onUpdate: 'CASCADE',
            }),
        );
    }

    async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.dropForeignKey('comments', 'FK_d6f93329801a93536da4241e386');
        await queryRunner.dropColumn('comments', 'parent_id');

        await queryRunner.dropForeignKey('comments', 'FK_e7333f3b6b002419e56808ff911');
        await queryRunner.dropColumn('comments', 'reply_user_id');
    }
}
