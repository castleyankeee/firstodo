import {Entity, PrimaryGeneratedColumn, Column, ManyToOne, JoinColumn} from 'typeorm';
import {UsersEntity} from './users.entity';
import {WorkspacesEntity} from './workspaces.entity';
import { RoleEnum } from '../enums/role.enum';

@Entity({ name: 'workspace_users' })
export class WorkspaceUsersEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({
    type: 'enum',
    enum: RoleEnum,
    default: RoleEnum.ADMIN
  })
  role: RoleEnum;

  @Column()
  user_id: string;

  @Column()
  workspace_id: string;

  @ManyToOne(() => UsersEntity, (user) => user.workspaceUsers)
  @JoinColumn({ name: 'user_id' })
  user: UsersEntity;



  @ManyToOne(() => WorkspacesEntity, (workspace) => workspace.workspaceUsers)
  @JoinColumn({ name: 'workspace_id' })
  workspace: WorkspacesEntity;


}
