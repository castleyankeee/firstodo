import {Entity, PrimaryGeneratedColumn, Column, OneToMany, ManyToOne, JoinColumn} from 'typeorm';
import {CardLabelsEntity} from './card-labels.entity';
import {WorkspacesEntity} from './workspaces.entity';

@Entity({ name: 'labels' })
export class LabelsEntity {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @OneToMany(() => CardLabelsEntity, (cardLabel) => cardLabel.label)
  cardLabels: CardLabelsEntity[];

  @ManyToOne(() => WorkspacesEntity, (workspace) => workspace.labels)
  @JoinColumn({ name: 'workspace_id' })
  workspace: WorkspacesEntity;

  @Column()
  workspace_id: string;
}
