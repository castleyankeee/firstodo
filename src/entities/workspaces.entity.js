"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkspacesEntity = void 0;
var typeorm_1 = require("typeorm");
var workspace_users_entity_1 = require("./workspace-users.entity");
var lists_entity_1 = require("./lists.entity");
var labels_entity_1 = require("./labels.entity");
var valability_enum_1 = require("../enums/valability.enum");
var WorkspacesEntity = exports.WorkspacesEntity = function () {
    var _classDecorators = [(0, typeorm_1.Entity)()];
    var _classDescriptor;
    var _classExtraInitializers = [];
    var _classThis;
    var _instanceExtraInitializers = [];
    var _id_decorators;
    var _id_initializers = [];
    var _name_decorators;
    var _name_initializers = [];
    var _background_img_url_decorators;
    var _background_img_url_initializers = [];
    var _valability_decorators;
    var _valability_initializers = [];
    var _created_at_decorators;
    var _created_at_initializers = [];
    var _updated_at_decorators;
    var _updated_at_initializers = [];
    var _workspaceUsers_decorators;
    var _workspaceUsers_initializers = [];
    var _lists_decorators;
    var _lists_initializers = [];
    var _labels_decorators;
    var _labels_initializers = [];
    var WorkspacesEntity = _classThis = /** @class */ (function () {
        function WorkspacesEntity_1() {
            this.id = (__runInitializers(this, _instanceExtraInitializers), __runInitializers(this, _id_initializers, void 0));
            this.name = __runInitializers(this, _name_initializers, void 0);
            this.background_img_url = __runInitializers(this, _background_img_url_initializers, void 0);
            this.valability = __runInitializers(this, _valability_initializers, void 0);
            this.created_at = __runInitializers(this, _created_at_initializers, void 0);
            this.updated_at = __runInitializers(this, _updated_at_initializers, void 0);
            this.workspaceUsers = __runInitializers(this, _workspaceUsers_initializers, void 0);
            this.lists = __runInitializers(this, _lists_initializers, void 0);
            this.labels = __runInitializers(this, _labels_initializers, void 0);
        }
        return WorkspacesEntity_1;
    }());
    __setFunctionName(_classThis, "WorkspacesEntity");
    (function () {
        _id_decorators = [(0, typeorm_1.PrimaryGeneratedColumn)('uuid')];
        _name_decorators = [(0, typeorm_1.Column)()];
        _background_img_url_decorators = [(0, typeorm_1.Column)()];
        _valability_decorators = [(0, typeorm_1.Column)({ type: 'enum', enum: valability_enum_1.ValabilityEnum })];
        _created_at_decorators = [(0, typeorm_1.CreateDateColumn)()];
        _updated_at_decorators = [(0, typeorm_1.UpdateDateColumn)()];
        _workspaceUsers_decorators = [(0, typeorm_1.OneToMany)(function () { return workspace_users_entity_1.WorkspaceUsersEntity; }, function (workspaceUser) { return workspaceUser.workspace; })];
        _lists_decorators = [(0, typeorm_1.OneToMany)(function () { return lists_entity_1.ListsEntity; }, function (list) { return list.workspace; })];
        _labels_decorators = [(0, typeorm_1.OneToMany)(function () { return labels_entity_1.LabelsEntity; }, function (label) { return label.workspace; })];
        __esDecorate(null, null, _id_decorators, { kind: "field", name: "id", static: false, private: false, access: { has: function (obj) { return "id" in obj; }, get: function (obj) { return obj.id; }, set: function (obj, value) { obj.id = value; } } }, _id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _name_decorators, { kind: "field", name: "name", static: false, private: false, access: { has: function (obj) { return "name" in obj; }, get: function (obj) { return obj.name; }, set: function (obj, value) { obj.name = value; } } }, _name_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _background_img_url_decorators, { kind: "field", name: "background_img_url", static: false, private: false, access: { has: function (obj) { return "background_img_url" in obj; }, get: function (obj) { return obj.background_img_url; }, set: function (obj, value) { obj.background_img_url = value; } } }, _background_img_url_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _valability_decorators, { kind: "field", name: "valability", static: false, private: false, access: { has: function (obj) { return "valability" in obj; }, get: function (obj) { return obj.valability; }, set: function (obj, value) { obj.valability = value; } } }, _valability_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _created_at_decorators, { kind: "field", name: "created_at", static: false, private: false, access: { has: function (obj) { return "created_at" in obj; }, get: function (obj) { return obj.created_at; }, set: function (obj, value) { obj.created_at = value; } } }, _created_at_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _updated_at_decorators, { kind: "field", name: "updated_at", static: false, private: false, access: { has: function (obj) { return "updated_at" in obj; }, get: function (obj) { return obj.updated_at; }, set: function (obj, value) { obj.updated_at = value; } } }, _updated_at_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _workspaceUsers_decorators, { kind: "field", name: "workspaceUsers", static: false, private: false, access: { has: function (obj) { return "workspaceUsers" in obj; }, get: function (obj) { return obj.workspaceUsers; }, set: function (obj, value) { obj.workspaceUsers = value; } } }, _workspaceUsers_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _lists_decorators, { kind: "field", name: "lists", static: false, private: false, access: { has: function (obj) { return "lists" in obj; }, get: function (obj) { return obj.lists; }, set: function (obj, value) { obj.lists = value; } } }, _lists_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _labels_decorators, { kind: "field", name: "labels", static: false, private: false, access: { has: function (obj) { return "labels" in obj; }, get: function (obj) { return obj.labels; }, set: function (obj, value) { obj.labels = value; } } }, _labels_initializers, _instanceExtraInitializers);
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name }, null, _classExtraInitializers);
        WorkspacesEntity = _classThis = _classDescriptor.value;
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return WorkspacesEntity = _classThis;
}();
