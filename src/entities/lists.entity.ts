import {Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany, ManyToOne, JoinColumn} from 'typeorm';
import{CardsEntity} from './cards.entity';
import {WorkspacesEntity} from './workspaces.entity';

@Entity({ name: 'lists' })
export class ListsEntity {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column()
  order: Number;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(() => CardsEntity, (card) => card.list)
  cards: CardsEntity[];

  @ManyToOne(() => WorkspacesEntity, (workspace) => workspace.lists)
  @JoinColumn({ name: 'workspace_id' })
  workspace: WorkspacesEntity;

  @Column()
  workspace_id: string;
}
