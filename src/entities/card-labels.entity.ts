import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';
import {CardsEntity} from './cards.entity';
import { LabelsEntity } from './labels.entity';

@Entity({ name: 'card_labels' })
export class CardLabelsEntity {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  order: number;

  @ManyToOne(() => CardsEntity, (card) => card.cardLabels)
  @JoinColumn({ name: 'card_id' })
  card: CardsEntity;

  @Column()
  card_id: string;

  @ManyToOne(() => LabelsEntity, (label) => label.cardLabels)
  @JoinColumn({ name: 'label_id' })
  label: LabelsEntity;

  @Column()
  label_id: string;
}
