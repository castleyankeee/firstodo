"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardsEntity = void 0;
var typeorm_1 = require("typeorm");
var comments_entity_1 = require("./comments.entity");
var card_users_entity_1 = require("./card-users.entity");
var card_labels_entity_1 = require("./card-labels.entity");
var lists_entity_1 = require("./lists.entity");
var CardsEntity = exports.CardsEntity = function () {
    var _classDecorators = [(0, typeorm_1.Entity)()];
    var _classDescriptor;
    var _classExtraInitializers = [];
    var _classThis;
    var _instanceExtraInitializers = [];
    var _id_decorators;
    var _id_initializers = [];
    var _description_decorators;
    var _description_initializers = [];
    var _due_date_decorators;
    var _due_date_initializers = [];
    var _created_at_decorators;
    var _created_at_initializers = [];
    var _updated_at_decorators;
    var _updated_at_initializers = [];
    var _comments_decorators;
    var _comments_initializers = [];
    var _cardUsers_decorators;
    var _cardUsers_initializers = [];
    var _cardLabels_decorators;
    var _cardLabels_initializers = [];
    var _list_decorators;
    var _list_initializers = [];
    var _list_id_decorators;
    var _list_id_initializers = [];
    var CardsEntity = _classThis = /** @class */ (function () {
        function CardsEntity_1() {
            this.id = (__runInitializers(this, _instanceExtraInitializers), __runInitializers(this, _id_initializers, void 0));
            this.description = __runInitializers(this, _description_initializers, void 0);
            this.due_date = __runInitializers(this, _due_date_initializers, void 0);
            this.created_at = __runInitializers(this, _created_at_initializers, void 0);
            this.updated_at = __runInitializers(this, _updated_at_initializers, void 0);
            this.comments = __runInitializers(this, _comments_initializers, void 0);
            this.cardUsers = __runInitializers(this, _cardUsers_initializers, void 0);
            this.cardLabels = __runInitializers(this, _cardLabels_initializers, void 0);
            this.list = __runInitializers(this, _list_initializers, void 0);
            this.list_id = __runInitializers(this, _list_id_initializers, void 0);
        }
        return CardsEntity_1;
    }());
    __setFunctionName(_classThis, "CardsEntity");
    (function () {
        _id_decorators = [(0, typeorm_1.PrimaryGeneratedColumn)('uuid')];
        _description_decorators = [(0, typeorm_1.Column)()];
        _due_date_decorators = [(0, typeorm_1.Column)({
                type: 'date',
                nullable: true,
            })];
        _created_at_decorators = [(0, typeorm_1.CreateDateColumn)()];
        _updated_at_decorators = [(0, typeorm_1.UpdateDateColumn)()];
        _comments_decorators = [(0, typeorm_1.OneToMany)(function () { return comments_entity_1.CommentsEntity; }, function (comment) { return comment.card; })];
        _cardUsers_decorators = [(0, typeorm_1.OneToMany)(function () { return card_users_entity_1.CardUsersEntity; }, function (cardUser) { return cardUser.card; })];
        _cardLabels_decorators = [(0, typeorm_1.OneToMany)(function () { return card_labels_entity_1.CardLabelsEntity; }, function (cardLabel) { return cardLabel.card; })];
        _list_decorators = [(0, typeorm_1.ManyToOne)(function () { return lists_entity_1.ListsEntity; }, function (list) { return list.cards; }), (0, typeorm_1.JoinColumn)({ name: 'list_id' })];
        _list_id_decorators = [(0, typeorm_1.Column)()];
        __esDecorate(null, null, _id_decorators, { kind: "field", name: "id", static: false, private: false, access: { has: function (obj) { return "id" in obj; }, get: function (obj) { return obj.id; }, set: function (obj, value) { obj.id = value; } } }, _id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _description_decorators, { kind: "field", name: "description", static: false, private: false, access: { has: function (obj) { return "description" in obj; }, get: function (obj) { return obj.description; }, set: function (obj, value) { obj.description = value; } } }, _description_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _due_date_decorators, { kind: "field", name: "due_date", static: false, private: false, access: { has: function (obj) { return "due_date" in obj; }, get: function (obj) { return obj.due_date; }, set: function (obj, value) { obj.due_date = value; } } }, _due_date_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _created_at_decorators, { kind: "field", name: "created_at", static: false, private: false, access: { has: function (obj) { return "created_at" in obj; }, get: function (obj) { return obj.created_at; }, set: function (obj, value) { obj.created_at = value; } } }, _created_at_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _updated_at_decorators, { kind: "field", name: "updated_at", static: false, private: false, access: { has: function (obj) { return "updated_at" in obj; }, get: function (obj) { return obj.updated_at; }, set: function (obj, value) { obj.updated_at = value; } } }, _updated_at_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _comments_decorators, { kind: "field", name: "comments", static: false, private: false, access: { has: function (obj) { return "comments" in obj; }, get: function (obj) { return obj.comments; }, set: function (obj, value) { obj.comments = value; } } }, _comments_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _cardUsers_decorators, { kind: "field", name: "cardUsers", static: false, private: false, access: { has: function (obj) { return "cardUsers" in obj; }, get: function (obj) { return obj.cardUsers; }, set: function (obj, value) { obj.cardUsers = value; } } }, _cardUsers_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _cardLabels_decorators, { kind: "field", name: "cardLabels", static: false, private: false, access: { has: function (obj) { return "cardLabels" in obj; }, get: function (obj) { return obj.cardLabels; }, set: function (obj, value) { obj.cardLabels = value; } } }, _cardLabels_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _list_decorators, { kind: "field", name: "list", static: false, private: false, access: { has: function (obj) { return "list" in obj; }, get: function (obj) { return obj.list; }, set: function (obj, value) { obj.list = value; } } }, _list_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _list_id_decorators, { kind: "field", name: "list_id", static: false, private: false, access: { has: function (obj) { return "list_id" in obj; }, get: function (obj) { return obj.list_id; }, set: function (obj, value) { obj.list_id = value; } } }, _list_id_initializers, _instanceExtraInitializers);
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name }, null, _classExtraInitializers);
        CardsEntity = _classThis = _classDescriptor.value;
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return CardsEntity = _classThis;
}();
