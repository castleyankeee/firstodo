"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
exports.WorkspaceUsersEntity = void 0;
var typeorm_1 = require("typeorm");
var users_entity_1 = require("./users.entity");
var workspaces_entity_1 = require("./workspaces.entity");
var role_enum_1 = require("../enums/role.enum");
var WorkspaceUsersEntity = exports.WorkspaceUsersEntity = function () {
    var _classDecorators = [(0, typeorm_1.Entity)()];
    var _classDescriptor;
    var _classExtraInitializers = [];
    var _classThis;
    var _instanceExtraInitializers = [];
    var _id_decorators;
    var _id_initializers = [];
    var _role_decorators;
    var _role_initializers = [];
    var _user_decorators;
    var _user_initializers = [];
    var _user_id_decorators;
    var _user_id_initializers = [];
    var _workspace_decorators;
    var _workspace_initializers = [];
    var _workspace_id_decorators;
    var _workspace_id_initializers = [];
    var WorkspaceUsersEntity = _classThis = /** @class */ (function () {
        function WorkspaceUsersEntity_1() {
            this.id = (__runInitializers(this, _instanceExtraInitializers), __runInitializers(this, _id_initializers, void 0));
            this.role = __runInitializers(this, _role_initializers, void 0);
            this.user = __runInitializers(this, _user_initializers, void 0);
            this.user_id = __runInitializers(this, _user_id_initializers, void 0);
            this.workspace = __runInitializers(this, _workspace_initializers, void 0);
            this.workspace_id = __runInitializers(this, _workspace_id_initializers, void 0);
        }
        return WorkspaceUsersEntity_1;
    }());
    __setFunctionName(_classThis, "WorkspaceUsersEntity");
    (function () {
        _id_decorators = [(0, typeorm_1.PrimaryGeneratedColumn)('uuid')];
        _role_decorators = [(0, typeorm_1.Column)({
                type: 'enum',
                enum: role_enum_1.RoleEnum,
            })];
        _user_decorators = [(0, typeorm_1.ManyToOne)(function () { return users_entity_1.UsersEntity; }, function (user) { return user.workspaceUsers; }), (0, typeorm_1.JoinColumn)({ name: 'user_id' })];
        _user_id_decorators = [(0, typeorm_1.Column)()];
        _workspace_decorators = [(0, typeorm_1.ManyToOne)(function () { return workspaces_entity_1.WorkspacesEntity; }, function (workspace) { return workspace.workspaceUsers; }), (0, typeorm_1.JoinColumn)({ name: 'workspace_id' })];
        _workspace_id_decorators = [(0, typeorm_1.Column)()];
        __esDecorate(null, null, _id_decorators, { kind: "field", name: "id", static: false, private: false, access: { has: function (obj) { return "id" in obj; }, get: function (obj) { return obj.id; }, set: function (obj, value) { obj.id = value; } } }, _id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _role_decorators, { kind: "field", name: "role", static: false, private: false, access: { has: function (obj) { return "role" in obj; }, get: function (obj) { return obj.role; }, set: function (obj, value) { obj.role = value; } } }, _role_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _user_decorators, { kind: "field", name: "user", static: false, private: false, access: { has: function (obj) { return "user" in obj; }, get: function (obj) { return obj.user; }, set: function (obj, value) { obj.user = value; } } }, _user_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _user_id_decorators, { kind: "field", name: "user_id", static: false, private: false, access: { has: function (obj) { return "user_id" in obj; }, get: function (obj) { return obj.user_id; }, set: function (obj, value) { obj.user_id = value; } } }, _user_id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _workspace_decorators, { kind: "field", name: "workspace", static: false, private: false, access: { has: function (obj) { return "workspace" in obj; }, get: function (obj) { return obj.workspace; }, set: function (obj, value) { obj.workspace = value; } } }, _workspace_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _workspace_id_decorators, { kind: "field", name: "workspace_id", static: false, private: false, access: { has: function (obj) { return "workspace_id" in obj; }, get: function (obj) { return obj.workspace_id; }, set: function (obj, value) { obj.workspace_id = value; } } }, _workspace_id_initializers, _instanceExtraInitializers);
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name }, null, _classExtraInitializers);
        WorkspaceUsersEntity = _classThis = _classDescriptor.value;
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return WorkspaceUsersEntity = _classThis;
}();
