import {Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany} from 'typeorm';
import {WorkspaceUsersEntity} from './workspace-users.entity';
import {ListsEntity} from './lists.entity';
import {LabelsEntity} from './labels.entity';
import { VisibilityEnum } from '../enums/visibility.enum';

@Entity({ name: 'workspace' })
export class WorkspacesEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  name: string;

  @Column({ nullable: true })
  background_img_url: string;

  @Column({ type: 'enum', enum: VisibilityEnum })
  visibility: VisibilityEnum;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(() => WorkspaceUsersEntity, (workspaceUser) => workspaceUser.workspace)
  workspaceUsers: WorkspaceUsersEntity[];

  @OneToMany(() => ListsEntity, (list) => list.workspace)
  lists: ListsEntity[];

  @OneToMany(() => LabelsEntity, (label) => label.workspace)
  labels: LabelsEntity[];
}
