import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  OneToMany,
  CreateDateColumn,
  UpdateDateColumn,
} from 'typeorm';
import {CardUsersEntity} from './card-users.entity';
import {WorkspaceUsersEntity} from './workspace-users.entity';
import {CommentsEntity} from './comments.entity';
import { RoleEnum } from '../enums/role.enum';

@Entity({ name: 'users' })
export class UsersEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  first_name: string;

  @Column()
  last_name: string;

  @Column({
    type: 'date',
  })
  date_of_birth: Date;

  @Column({ unique: true })
  email: string;

  @Column()
  password: string;

  @Column({ nullable: true })
  avatar_url: String;

  @Column({
    type: 'enum',
    enum: RoleEnum,
    default: RoleEnum.DEVELOPER,
  })
  role: RoleEnum;

  @OneToMany(() => CardUsersEntity, (cardUsers) => cardUsers.user)
  cardUsers: CardUsersEntity[];

  @OneToMany(() => WorkspaceUsersEntity, (workspaceUser) => workspaceUser.user)
  workspaceUsers: WorkspaceUsersEntity[];

  @OneToMany(() => CommentsEntity, (comment) => comment.user)
  comments: CommentsEntity[];

  @OneToMany(() => CommentsEntity, (replyComment) => replyComment.replyUser)
  replyComments: CommentsEntity[];

  @OneToMany(() => CommentsEntity, (parentComment) => parentComment.parentComment)
  parentComments: CommentsEntity[];


  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  update_at: Date;
}
