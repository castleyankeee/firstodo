import {Entity, Column, PrimaryGeneratedColumn, ManyToOne, JoinColumn} from 'typeorm';
import { CardsEntity } from './cards.entity';
import { UsersEntity } from './users.entity';

@Entity({ name: 'card_users' })
export class CardUsersEntity {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column({ nullable:true })
  order: string;

  @ManyToOne(() => CardsEntity, (card) => card.cardUsers)
  @JoinColumn({ name: 'card_id' })
  card: CardsEntity;

  @Column()
  card_id: string;

  @ManyToOne(() => UsersEntity, (user) => user.cardUsers)
  @JoinColumn({ name: 'user_id' })
  user: UsersEntity;

  @Column()
  user_id: string;
}