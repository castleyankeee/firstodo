"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CommentsEntity = void 0;
var typeorm_1 = require("typeorm");
var cards_entity_1 = require("./cards.entity");
var users_entity_1 = require("./users.entity");
var valability_enum_1 = require("../enums/valability.enum");
var card_labels_entity_1 = require("./card-labels.entity");
var CommentsEntity = exports.CommentsEntity = function () {
    var _classDecorators = [(0, typeorm_1.Entity)()];
    var _classDescriptor;
    var _classExtraInitializers = [];
    var _classThis;
    var _instanceExtraInitializers = [];
    var _id_decorators;
    var _id_initializers = [];
    var _activity_decorators;
    var _activity_initializers = [];
    var _content_decorators;
    var _content_initializers = [];
    var _valability_decorators;
    var _valability_initializers = [];
    var _created_at_decorators;
    var _created_at_initializers = [];
    var _updated_at_decorators;
    var _updated_at_initializers = [];
    var _card_decorators;
    var _card_initializers = [];
    var _card_id_decorators;
    var _card_id_initializers = [];
    var _user_decorators;
    var _user_initializers = [];
    var _user_id_decorators;
    var _user_id_initializers = [];
    var _cardLabels_decorators;
    var _cardLabels_initializers = [];
    var CommentsEntity = _classThis = /** @class */ (function () {
        function CommentsEntity_1() {
            this.id = (__runInitializers(this, _instanceExtraInitializers), __runInitializers(this, _id_initializers, void 0));
            this.activity = __runInitializers(this, _activity_initializers, void 0);
            this.content = __runInitializers(this, _content_initializers, void 0);
            this.valability = __runInitializers(this, _valability_initializers, void 0);
            this.created_at = __runInitializers(this, _created_at_initializers, void 0);
            this.updated_at = __runInitializers(this, _updated_at_initializers, void 0);
            this.card = __runInitializers(this, _card_initializers, void 0);
            this.card_id = __runInitializers(this, _card_id_initializers, void 0);
            this.user = __runInitializers(this, _user_initializers, void 0);
            this.user_id = __runInitializers(this, _user_id_initializers, void 0);
            this.cardLabels = __runInitializers(this, _cardLabels_initializers, void 0);
        }
        return CommentsEntity_1;
    }());
    __setFunctionName(_classThis, "CommentsEntity");
    (function () {
        _id_decorators = [(0, typeorm_1.PrimaryGeneratedColumn)('uuid')];
        _activity_decorators = [(0, typeorm_1.Column)()];
        _content_decorators = [(0, typeorm_1.Column)()];
        _valability_decorators = [(0, typeorm_1.Column)({
                type: 'enum',
                enum: valability_enum_1.ValabilityEnum,
            })];
        _created_at_decorators = [(0, typeorm_1.CreateDateColumn)()];
        _updated_at_decorators = [(0, typeorm_1.UpdateDateColumn)()];
        _card_decorators = [(0, typeorm_1.ManyToOne)(function () { return cards_entity_1.CardsEntity; }, function (card) { return card.comments; }), (0, typeorm_1.JoinColumn)({ name: 'card_id' })];
        _card_id_decorators = [(0, typeorm_1.Column)()];
        _user_decorators = [(0, typeorm_1.ManyToOne)(function () { return users_entity_1.UsersEntity; }, function (user) { return user.comments; }), (0, typeorm_1.JoinColumn)({ name: 'user_id' })];
        _user_id_decorators = [(0, typeorm_1.Column)()];
        _cardLabels_decorators = [(0, typeorm_1.OneToMany)(function () { return card_labels_entity_1.CardLabelsEntity; }, function (cardLabel) { return cardLabel.comment; })];
        __esDecorate(null, null, _id_decorators, { kind: "field", name: "id", static: false, private: false, access: { has: function (obj) { return "id" in obj; }, get: function (obj) { return obj.id; }, set: function (obj, value) { obj.id = value; } } }, _id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _activity_decorators, { kind: "field", name: "activity", static: false, private: false, access: { has: function (obj) { return "activity" in obj; }, get: function (obj) { return obj.activity; }, set: function (obj, value) { obj.activity = value; } } }, _activity_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _content_decorators, { kind: "field", name: "content", static: false, private: false, access: { has: function (obj) { return "content" in obj; }, get: function (obj) { return obj.content; }, set: function (obj, value) { obj.content = value; } } }, _content_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _valability_decorators, { kind: "field", name: "valability", static: false, private: false, access: { has: function (obj) { return "valability" in obj; }, get: function (obj) { return obj.valability; }, set: function (obj, value) { obj.valability = value; } } }, _valability_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _created_at_decorators, { kind: "field", name: "created_at", static: false, private: false, access: { has: function (obj) { return "created_at" in obj; }, get: function (obj) { return obj.created_at; }, set: function (obj, value) { obj.created_at = value; } } }, _created_at_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _updated_at_decorators, { kind: "field", name: "updated_at", static: false, private: false, access: { has: function (obj) { return "updated_at" in obj; }, get: function (obj) { return obj.updated_at; }, set: function (obj, value) { obj.updated_at = value; } } }, _updated_at_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _card_decorators, { kind: "field", name: "card", static: false, private: false, access: { has: function (obj) { return "card" in obj; }, get: function (obj) { return obj.card; }, set: function (obj, value) { obj.card = value; } } }, _card_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _card_id_decorators, { kind: "field", name: "card_id", static: false, private: false, access: { has: function (obj) { return "card_id" in obj; }, get: function (obj) { return obj.card_id; }, set: function (obj, value) { obj.card_id = value; } } }, _card_id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _user_decorators, { kind: "field", name: "user", static: false, private: false, access: { has: function (obj) { return "user" in obj; }, get: function (obj) { return obj.user; }, set: function (obj, value) { obj.user = value; } } }, _user_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _user_id_decorators, { kind: "field", name: "user_id", static: false, private: false, access: { has: function (obj) { return "user_id" in obj; }, get: function (obj) { return obj.user_id; }, set: function (obj, value) { obj.user_id = value; } } }, _user_id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _cardLabels_decorators, { kind: "field", name: "cardLabels", static: false, private: false, access: { has: function (obj) { return "cardLabels" in obj; }, get: function (obj) { return obj.cardLabels; }, set: function (obj, value) { obj.cardLabels = value; } } }, _cardLabels_initializers, _instanceExtraInitializers);
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name }, null, _classExtraInitializers);
        CommentsEntity = _classThis = _classDescriptor.value;
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return CommentsEntity = _classThis;
}();
