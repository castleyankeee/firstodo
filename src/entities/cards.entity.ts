import {Entity, Column, PrimaryGeneratedColumn, CreateDateColumn, UpdateDateColumn, OneToMany, ManyToOne, JoinColumn } from 'typeorm';
import {CommentsEntity} from './comments.entity';
import {CardUsersEntity} from './card-users.entity';
import {CardLabelsEntity} from './card-labels.entity';
import {ListsEntity} from './lists.entity';

@Entity({ name: 'cards' })
export class CardsEntity {

  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  description: string;

  @Column({
    type:'date',
    nullable:true
  })
  due_date: Date;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @OneToMany(() => CommentsEntity, (comment) => comment.card)
  comments: CommentsEntity[];

  @OneToMany(() => CardUsersEntity, (cardUser) => cardUser.card)
  cardUsers: CardUsersEntity[];

  @OneToMany(() => CardLabelsEntity, (cardLabel) => cardLabel.card)
  cardLabels: CardLabelsEntity[];

  @ManyToOne(() => ListsEntity, (list) => list.cards)
  @JoinColumn({ name: 'list_id' })
  list: ListsEntity;

  @Column()
  list_id: string;
}
