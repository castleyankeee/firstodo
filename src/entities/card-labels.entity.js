"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
exports.CardLabelsEntity = void 0;
var typeorm_1 = require("typeorm");
var cards_entity_1 = require("./cards.entity");
var comments_entity_1 = require("./comments.entity");
var labels_entity_1 = require("./labels.entity");
var CardLabelsEntity = exports.CardLabelsEntity = function () {
    var _classDecorators = [(0, typeorm_1.Entity)()];
    var _classDescriptor;
    var _classExtraInitializers = [];
    var _classThis;
    var _instanceExtraInitializers = [];
    var _id_decorators;
    var _id_initializers = [];
    var _order_decorators;
    var _order_initializers = [];
    var _card_decorators;
    var _card_initializers = [];
    var _card_id_decorators;
    var _card_id_initializers = [];
    var _comment_decorators;
    var _comment_initializers = [];
    var _comment_id_decorators;
    var _comment_id_initializers = [];
    var _label_decorators;
    var _label_initializers = [];
    var _label_id_decorators;
    var _label_id_initializers = [];
    var CardLabelsEntity = _classThis = /** @class */ (function () {
        function CardLabelsEntity_1() {
            this.id = (__runInitializers(this, _instanceExtraInitializers), __runInitializers(this, _id_initializers, void 0));
            this.order = __runInitializers(this, _order_initializers, void 0);
            this.card = __runInitializers(this, _card_initializers, void 0);
            this.card_id = __runInitializers(this, _card_id_initializers, void 0);
            this.comment = __runInitializers(this, _comment_initializers, void 0);
            this.comment_id = __runInitializers(this, _comment_id_initializers, void 0);
            this.label = __runInitializers(this, _label_initializers, void 0);
            this.label_id = __runInitializers(this, _label_id_initializers, void 0);
        }
        return CardLabelsEntity_1;
    }());
    __setFunctionName(_classThis, "CardLabelsEntity");
    (function () {
        _id_decorators = [(0, typeorm_1.PrimaryGeneratedColumn)('uuid')];
        _order_decorators = [(0, typeorm_1.Column)()];
        _card_decorators = [(0, typeorm_1.ManyToOne)(function () { return cards_entity_1.CardsEntity; }, function (card) { return card.cardLabels; }), (0, typeorm_1.JoinColumn)({ name: 'card_id' })];
        _card_id_decorators = [(0, typeorm_1.Column)()];
        _comment_decorators = [(0, typeorm_1.ManyToOne)(function () { return comments_entity_1.CommentsEntity; }, function (comment) { return comment.cardLabels; }), (0, typeorm_1.JoinColumn)({ name: 'comment_id' })];
        _comment_id_decorators = [(0, typeorm_1.Column)()];
        _label_decorators = [(0, typeorm_1.ManyToOne)(function () { return labels_entity_1.LabelsEntity; }, function (label) { return label.cardLabels; }), (0, typeorm_1.JoinColumn)({ name: 'label_id' })];
        _label_id_decorators = [(0, typeorm_1.Column)()];
        __esDecorate(null, null, _id_decorators, { kind: "field", name: "id", static: false, private: false, access: { has: function (obj) { return "id" in obj; }, get: function (obj) { return obj.id; }, set: function (obj, value) { obj.id = value; } } }, _id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _order_decorators, { kind: "field", name: "order", static: false, private: false, access: { has: function (obj) { return "order" in obj; }, get: function (obj) { return obj.order; }, set: function (obj, value) { obj.order = value; } } }, _order_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _card_decorators, { kind: "field", name: "card", static: false, private: false, access: { has: function (obj) { return "card" in obj; }, get: function (obj) { return obj.card; }, set: function (obj, value) { obj.card = value; } } }, _card_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _card_id_decorators, { kind: "field", name: "card_id", static: false, private: false, access: { has: function (obj) { return "card_id" in obj; }, get: function (obj) { return obj.card_id; }, set: function (obj, value) { obj.card_id = value; } } }, _card_id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _comment_decorators, { kind: "field", name: "comment", static: false, private: false, access: { has: function (obj) { return "comment" in obj; }, get: function (obj) { return obj.comment; }, set: function (obj, value) { obj.comment = value; } } }, _comment_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _comment_id_decorators, { kind: "field", name: "comment_id", static: false, private: false, access: { has: function (obj) { return "comment_id" in obj; }, get: function (obj) { return obj.comment_id; }, set: function (obj, value) { obj.comment_id = value; } } }, _comment_id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _label_decorators, { kind: "field", name: "label", static: false, private: false, access: { has: function (obj) { return "label" in obj; }, get: function (obj) { return obj.label; }, set: function (obj, value) { obj.label = value; } } }, _label_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _label_id_decorators, { kind: "field", name: "label_id", static: false, private: false, access: { has: function (obj) { return "label_id" in obj; }, get: function (obj) { return obj.label_id; }, set: function (obj, value) { obj.label_id = value; } } }, _label_id_initializers, _instanceExtraInitializers);
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name }, null, _classExtraInitializers);
        CardLabelsEntity = _classThis = _classDescriptor.value;
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return CardLabelsEntity = _classThis;
}();
