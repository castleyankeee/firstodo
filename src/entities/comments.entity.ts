import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToOne,
  JoinColumn,
  OneToMany
} from 'typeorm';
import { CardsEntity } from './cards.entity';
import { UsersEntity } from './users.entity';
import { VisibilityEnum } from '../enums/visibility.enum';

@Entity({ name: 'comments' })
export class CommentsEntity {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column()
  activity: boolean;

  @Column()
  content: string;

  @Column({
    type: 'enum',
    enum: VisibilityEnum,
  })
  visibility: VisibilityEnum;

  @CreateDateColumn()
  created_at: Date;

  @UpdateDateColumn()
  updated_at: Date;

  @ManyToOne(() => CardsEntity, (card) => card.comments)
  @JoinColumn({ name: 'card_id' })
  card: CardsEntity;

  @Column()
  card_id: string;

  @ManyToOne(() => UsersEntity, (user) => user.comments)
  @JoinColumn({ name: 'user_id' })
  user: UsersEntity;

  @Column()
  user_id: string;

  @ManyToOne(() => CommentsEntity, (parentComment) => parentComment.childComments, { nullable: true })
  @JoinColumn({ name: 'parent_id' })
  parentComment: CommentsEntity;

  @Column({ nullable: true })
  parent_id: string;

  @ManyToOne(() => UsersEntity, (replyUser) => replyUser.replyComments, { nullable: true })
  @JoinColumn({ name: 'reply_user_id' })
  replyUser: UsersEntity;

  @Column({ nullable: true })
  reply_user_id: string;

  @OneToMany(() => CommentsEntity, (childComment) => childComment.parentComment)
  childComments: CommentsEntity[];

}


