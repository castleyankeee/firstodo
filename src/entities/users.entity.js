"use strict";
var __esDecorate = (this && this.__esDecorate) || function (ctor, descriptorIn, decorators, contextIn, initializers, extraInitializers) {
    function accept(f) { if (f !== void 0 && typeof f !== "function") throw new TypeError("Function expected"); return f; }
    var kind = contextIn.kind, key = kind === "getter" ? "get" : kind === "setter" ? "set" : "value";
    var target = !descriptorIn && ctor ? contextIn["static"] ? ctor : ctor.prototype : null;
    var descriptor = descriptorIn || (target ? Object.getOwnPropertyDescriptor(target, contextIn.name) : {});
    var _, done = false;
    for (var i = decorators.length - 1; i >= 0; i--) {
        var context = {};
        for (var p in contextIn) context[p] = p === "access" ? {} : contextIn[p];
        for (var p in contextIn.access) context.access[p] = contextIn.access[p];
        context.addInitializer = function (f) { if (done) throw new TypeError("Cannot add initializers after decoration has completed"); extraInitializers.push(accept(f || null)); };
        var result = (0, decorators[i])(kind === "accessor" ? { get: descriptor.get, set: descriptor.set } : descriptor[key], context);
        if (kind === "accessor") {
            if (result === void 0) continue;
            if (result === null || typeof result !== "object") throw new TypeError("Object expected");
            if (_ = accept(result.get)) descriptor.get = _;
            if (_ = accept(result.set)) descriptor.set = _;
            if (_ = accept(result.init)) initializers.unshift(_);
        }
        else if (_ = accept(result)) {
            if (kind === "field") initializers.unshift(_);
            else descriptor[key] = _;
        }
    }
    if (target) Object.defineProperty(target, contextIn.name, descriptor);
    done = true;
};
var __runInitializers = (this && this.__runInitializers) || function (thisArg, initializers, value) {
    var useValue = arguments.length > 2;
    for (var i = 0; i < initializers.length; i++) {
        value = useValue ? initializers[i].call(thisArg, value) : initializers[i].call(thisArg);
    }
    return useValue ? value : void 0;
};
var __setFunctionName = (this && this.__setFunctionName) || function (f, name, prefix) {
    if (typeof name === "symbol") name = name.description ? "[".concat(name.description, "]") : "";
    return Object.defineProperty(f, "name", { configurable: true, value: prefix ? "".concat(prefix, " ", name) : name });
};
var _this = this;
Object.defineProperty(exports, "__esModule", { value: true });
exports.UsersEntity = void 0;
var typeorm_1 = require("typeorm");
var card_users_entity_1 = require("./card-users.entity");
var workspace_users_entity_1 = require("./workspace-users.entity");
var comments_entity_1 = require("./comments.entity");
var role_enum_1 = require("../enums/role.enum");
var UsersEntity = exports.UsersEntity = function () {
    var _classDecorators = [(0, typeorm_1.Entity)()];
    var _classDescriptor;
    var _classExtraInitializers = [];
    var _classThis;
    var _instanceExtraInitializers = [];
    var _id_decorators;
    var _id_initializers = [];
    var _first_name_decorators;
    var _first_name_initializers = [];
    var _last_name_decorators;
    var _last_name_initializers = [];
    var _date_of_birth_decorators;
    var _date_of_birth_initializers = [];
    var _email_decorators;
    var _email_initializers = [];
    var _password_decorators;
    var _password_initializers = [];
    var _avatar_url_decorators;
    var _avatar_url_initializers = [];
    var _created_at_decorators;
    var _created_at_initializers = [];
    var _update_at_decorators;
    var _update_at_initializers = [];
    var _role_decorators;
    var _role_initializers = [];
    var _cardUsers_decorators;
    var _cardUsers_initializers = [];
    var _workspaceUsers_decorators;
    var _workspaceUsers_initializers = [];
    var _comments_decorators;
    var _comments_initializers = [];
    var UsersEntity = _classThis = /** @class */ (function () {
        function UsersEntity_1() {
            this.id = (__runInitializers(this, _instanceExtraInitializers), __runInitializers(this, _id_initializers, void 0));
            this.first_name = __runInitializers(this, _first_name_initializers, void 0);
            this.last_name = __runInitializers(this, _last_name_initializers, void 0);
            this.date_of_birth = __runInitializers(this, _date_of_birth_initializers, void 0);
            this.email = __runInitializers(this, _email_initializers, void 0);
            this.password = __runInitializers(this, _password_initializers, void 0);
            this.avatar_url = __runInitializers(this, _avatar_url_initializers, void 0);
            this.created_at = __runInitializers(this, _created_at_initializers, void 0);
            this.update_at = __runInitializers(this, _update_at_initializers, void 0);
            this.role = __runInitializers(this, _role_initializers, void 0);
            this.cardUsers = __runInitializers(this, _cardUsers_initializers, void 0);
            this.workspaceUsers = __runInitializers(this, _workspaceUsers_initializers, void 0);
            this.comments = __runInitializers(this, _comments_initializers, void 0);
        }
        return UsersEntity_1;
    }());
    __setFunctionName(_classThis, "UsersEntity");
    (function () {
        _id_decorators = [(0, typeorm_1.PrimaryGeneratedColumn)('uuid')];
        _first_name_decorators = [(0, typeorm_1.Column)()];
        _last_name_decorators = [(0, typeorm_1.Column)()];
        _date_of_birth_decorators = [(0, typeorm_1.Column)({
                type: 'date',
            })];
        _email_decorators = [(0, typeorm_1.Column)()];
        _password_decorators = [(0, typeorm_1.Column)()];
        _avatar_url_decorators = [(0, typeorm_1.Column)()];
        _created_at_decorators = [(0, typeorm_1.Column)()];
        _update_at_decorators = [(0, typeorm_1.Column)()];
        _role_decorators = [(0, typeorm_1.Column)({ type: 'enum', enum: role_enum_1.RoleEnum })];
        _cardUsers_decorators = [(0, typeorm_1.OneToMany)(function () { return card_users_entity_1.CardUsersEntity; }, function (cardUsers) { return cardUsers.user; })];
        _workspaceUsers_decorators = [(0, typeorm_1.OneToMany)(function () { return workspace_users_entity_1.WorkspaceUsersEntity; }, function (workspaceUser) { return workspaceUser.user; })];
        _comments_decorators = [(0, typeorm_1.OneToMany)(function () { return comments_entity_1.CommentsEntity; }, function (comment) { return comment.user; })];
        __esDecorate(null, null, _id_decorators, { kind: "field", name: "id", static: false, private: false, access: { has: function (obj) { return "id" in obj; }, get: function (obj) { return obj.id; }, set: function (obj, value) { obj.id = value; } } }, _id_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _first_name_decorators, { kind: "field", name: "first_name", static: false, private: false, access: { has: function (obj) { return "first_name" in obj; }, get: function (obj) { return obj.first_name; }, set: function (obj, value) { obj.first_name = value; } } }, _first_name_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _last_name_decorators, { kind: "field", name: "last_name", static: false, private: false, access: { has: function (obj) { return "last_name" in obj; }, get: function (obj) { return obj.last_name; }, set: function (obj, value) { obj.last_name = value; } } }, _last_name_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _date_of_birth_decorators, { kind: "field", name: "date_of_birth", static: false, private: false, access: { has: function (obj) { return "date_of_birth" in obj; }, get: function (obj) { return obj.date_of_birth; }, set: function (obj, value) { obj.date_of_birth = value; } } }, _date_of_birth_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _email_decorators, { kind: "field", name: "email", static: false, private: false, access: { has: function (obj) { return "email" in obj; }, get: function (obj) { return obj.email; }, set: function (obj, value) { obj.email = value; } } }, _email_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _password_decorators, { kind: "field", name: "password", static: false, private: false, access: { has: function (obj) { return "password" in obj; }, get: function (obj) { return obj.password; }, set: function (obj, value) { obj.password = value; } } }, _password_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _avatar_url_decorators, { kind: "field", name: "avatar_url", static: false, private: false, access: { has: function (obj) { return "avatar_url" in obj; }, get: function (obj) { return obj.avatar_url; }, set: function (obj, value) { obj.avatar_url = value; } } }, _avatar_url_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _created_at_decorators, { kind: "field", name: "created_at", static: false, private: false, access: { has: function (obj) { return "created_at" in obj; }, get: function (obj) { return obj.created_at; }, set: function (obj, value) { obj.created_at = value; } } }, _created_at_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _update_at_decorators, { kind: "field", name: "update_at", static: false, private: false, access: { has: function (obj) { return "update_at" in obj; }, get: function (obj) { return obj.update_at; }, set: function (obj, value) { obj.update_at = value; } } }, _update_at_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _role_decorators, { kind: "field", name: "role", static: false, private: false, access: { has: function (obj) { return "role" in obj; }, get: function (obj) { return obj.role; }, set: function (obj, value) { obj.role = value; } } }, _role_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _cardUsers_decorators, { kind: "field", name: "cardUsers", static: false, private: false, access: { has: function (obj) { return "cardUsers" in obj; }, get: function (obj) { return obj.cardUsers; }, set: function (obj, value) { obj.cardUsers = value; } } }, _cardUsers_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _workspaceUsers_decorators, { kind: "field", name: "workspaceUsers", static: false, private: false, access: { has: function (obj) { return "workspaceUsers" in obj; }, get: function (obj) { return obj.workspaceUsers; }, set: function (obj, value) { obj.workspaceUsers = value; } } }, _workspaceUsers_initializers, _instanceExtraInitializers);
        __esDecorate(null, null, _comments_decorators, { kind: "field", name: "comments", static: false, private: false, access: { has: function (obj) { return "comments" in obj; }, get: function (obj) { return obj.comments; }, set: function (obj, value) { obj.comments = value; } } }, _comments_initializers, _instanceExtraInitializers);
        __esDecorate(null, _classDescriptor = { value: _classThis }, _classDecorators, { kind: "class", name: _classThis.name }, null, _classExtraInitializers);
        UsersEntity = _classThis = _classDescriptor.value;
        __runInitializers(_classThis, _classExtraInitializers);
    })();
    return UsersEntity = _classThis;
}();
