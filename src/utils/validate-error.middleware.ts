import { validate, ValidationError } from 'class-validator';

export const validateError = async (request: object): Promise<ValidationError[]> => {
  return await validate(request, { forbidUnknownValues: false, enableDebugMessages: true });
};