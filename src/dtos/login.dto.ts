import {IsNotEmpty, IsString, IsEmail, IsLowercase} from 'class-validator';

export class LoginDTO {
  @IsLowercase()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  password: string;

  constructor(data = null) {
    this.email = data.email;
    this.password = data.password;
  }
}





