import {IsNotEmpty, IsString, IsEmail, IsDateString, IsLowercase,} from 'class-validator';

export class RegisterDTO {
  @IsNotEmpty()
  @IsString()
  first_name: string;

  @IsNotEmpty()
  @IsString()
  last_name: string;

  @IsDateString()
  date_of_birth: Date;

  @IsLowercase()
  @IsNotEmpty()
  @IsEmail()
  email: string;

  @IsNotEmpty()
  @IsString()
  password: string;

  constructor(data = null) {
    this.first_name = data.first_name;
    this.last_name = data.last_name;
    this.date_of_birth = data.date_of_birth;
    this.email = data.email;
    this.password = data.password;
  }
}
