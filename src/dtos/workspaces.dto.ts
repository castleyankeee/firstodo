import {IsNotEmpty, IsString, IsOptional, IsEnum} from 'class-validator';
import {VisibilityEnum} from '../enums/visibility.enum';

export class WorkspacesDto {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  background_img_URL: string;

  @IsEnum(VisibilityEnum)
  visibility: VisibilityEnum;

  constructor(data = null) {
    this.name = data.name;
    this.background_img_URL = data.background_img_URL;
    this.visibility = data.visibility;
  }
}