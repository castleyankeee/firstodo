import {IsBoolean, IsEnum, IsOptional, IsString, IsUUID} from 'class-validator';
import {VisibilityEnum} from '../enums/visibility.enum';


export class CommentsDTO {
  @IsBoolean()
  activity: boolean;

  @IsString()
  content: string;

  @IsEnum(VisibilityEnum)
  visibility: VisibilityEnum;

  @IsUUID()
  user_id: string;

  @IsUUID()
  card_id: string;

  @IsUUID()
  @IsOptional()
  parent_id: string;

  @IsUUID()
  @IsOptional()
  reply_user_id: string;

  constructor(data = null) {
      this.activity = data.activity;
      this.content = data.content;
      this.visibility = data.visibility;
      this.user_id = data.user_id;
      this.card_id = data.card_id;
      this.parent_id = data.parent_id;
      this.reply_user_id = data.reply_user_id;
  }
}
