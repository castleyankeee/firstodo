import {IsDateString, IsNotEmpty, IsOptional, IsString, IsUUID} from 'class-validator';



export class CardsDto {
  @IsNotEmpty()
  @IsString()
  description: string;

  @IsDateString()
  @IsOptional()
  due_date: Date;

  @IsUUID()
  list_id: string;

  constructor(data = null) {
    this.description = data.description;
    this.due_date = data.due_date;
    this.list_id = data.list_id;
  }
}