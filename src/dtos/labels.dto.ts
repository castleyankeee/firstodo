import {IsNotEmpty, IsString, IsUUID} from 'class-validator';

export class LabelsDTO{
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsUUID()
  workspace_id: string;

  constructor(data = null) {
    this.name = data.name;
    this.workspace_id = data.workspace_id;
  }
}

