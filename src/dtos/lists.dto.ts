import {IsNotEmpty, IsNumber, IsString, IsUUID} from 'class-validator';


export class ListsDTO {
  @IsNotEmpty()
  @IsString()
  name: string;

  @IsNotEmpty()
  @IsNumber()
  order: number;

  @IsUUID()
  workspace_id: string;

  constructor(data = null) {
    this.name = data.name;
    this.order = data.order;
    this.workspace_id = data.workspace_id;
  }
}
