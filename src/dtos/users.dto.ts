
import {IsNotEmpty, IsString, IsEmail, IsInt, IsLowercase} from 'class-validator';


export class UsersDto {
  @IsNotEmpty()
  @IsString()
  first_name!: string;

  @IsNotEmpty()
  @IsString()
  last_name!: string;

  @IsNotEmpty()
  @IsInt()
  date_of_birth: Date;

  @IsLowercase()
  @IsNotEmpty()
  @IsEmail()
  email!: string;
}
