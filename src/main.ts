import 'reflect-metadata';
import { AppDataSource } from '../data-source';
import { main } from './config';
const express = require('express');
import routes from './routes/index.router';
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('../openapi.json');

const app = express();


const bodyParser = require('body-parser');



  AppDataSource.initialize();


  app.use(express.json());
  app.use(bodyParser.json());
  app.use(routes);
  app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

  const port = main.port || 3000;
  app.listen(port, () => {
    console.log(`Server is running at http://localhost${port}`);
  });
