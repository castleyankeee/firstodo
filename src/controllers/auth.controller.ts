import {Request, Response} from 'express';
import {RegisterDTO} from '../dtos/register.dto';
import {LoginDTO} from '../dtos/login.dto';
import {validateError} from '../utils/validate-error.middleware';
import {AppDataSource} from '../../data-source';
import {UsersEntity} from '../entities/users.entity';
import {AuthenticationService} from '../services/auth.service';



const userRepository= AppDataSource.getRepository(UsersEntity);
export class AuthController {
  static async register(req: Request, res: Response) {
    try {
      const registerUserDto = new RegisterDTO(req.body);
      const errors = await validateError(registerUserDto);
      const user = await userRepository.findOneBy({ email: registerUserDto.email });

      if (errors.length) {
        res.status(400).json({
          success: false,
          errors,
        });
        return;
      }

      if (user) {
        res.status(404).send('User with that email already exists');
        return;
      }
      await AuthenticationService.register(registerUserDto);
      res.status(201).send('New users has been added');
    } catch (error) {
      res.status(500).json('Internal server error');
    }
  }

  static async login(req: Request, res: Response) {
    try {
      const loginDtoCreate = new LoginDTO(req.body);
      const errors = await validateError(loginDtoCreate);

      if (errors.length) {
        res.status(400).json({
          success: false,
          errors,
        });
        return;
      }
      const accessToken = await AuthenticationService.login(loginDtoCreate);
      if (!accessToken) {
        res.status(401).send('User or Password is wrong,try again!');
        return;
      }

      res.status(200).send(accessToken);
    } catch (error) {
      res.status(500).json('Internal server error');
    }
  }
}





