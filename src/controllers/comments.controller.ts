import {Request, Response} from 'express';
import {CommentsDTO} from '../dtos/comments.dto';
import {validateError} from '../utils/validate-error.middleware';
import {CommentsService} from '../services/comments.service';

export class CommentsController {
    static async createComment(req: Request, res: Response) {
        try {
            const createCommentsDto = new CommentsDTO(req.body);
            const errors = await validateError(createCommentsDto);
            if (errors.length) {
                res.status(400).json({
                    success: false,
                    errors,
                });
                return;
            }
            await CommentsService.createComment(createCommentsDto);
            res.status(200).send('Comment created');
            return;
        } catch (error) {
            res.status(500).send('Internal server error');
        }
    }

    static async getAllComments(req: Request, res: Response) {
        try {
            const page = parseInt(req.query.page as string) || 1;
            const limit = parseInt(req.query.limit as string) || 10;

            const comments = await CommentsService.getAllComments(page, limit);

            res.status(200).json({
                success: true,
                comments,
            });


        } catch (error) {
            res.status(500).send('Internal server error');
        }
    }
    static async editComment(req: Request, res:Response){
            const {commentId, content} = req.body;
        try{
            await CommentsService.editComments(commentId,content);
            res.status(200).send('Content has been edited')
            return
        }catch (error) {
            res.status(500).send('Internal server error')
        }
    }
    static async deleteComment(req:Request,res:Response) {
        const {id} = req.params;
        try {
            await CommentsService.deleteComment(id)
            res.status(200).send('Comment has been deleted')
            return
        }catch (error){
            res.status(500).send('Internal server error')
        }
    }
}
