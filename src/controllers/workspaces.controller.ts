import { Request,Response } from 'express';
import {WorkspacesDto} from '../dtos/workspaces.dto';
import {validateError} from '../utils/validate-error.middleware';
import {WorkspacesService} from '../services/workspaces.service';
import {UserRequest} from '../interfaces/user.interface';

export  class WorkspacesController {
  static async createWorkspace(req: UserRequest, res: Response) {

    try {
      const createWorkspaceDto = new WorkspacesDto(req.body);
      const errors = await validateError(createWorkspaceDto);
      if (errors.length) {
        res.status(400).json({
          success: false,
          errors,
        });
        return;
      }
      await WorkspacesService.createWorkspace(req.user.id, createWorkspaceDto);
      res.status(201).send('Workspace created');
      return;
    } catch (error) {
      res.status(500).json('Internal server error');
    }
  }
  static async getAllData(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const workspaceData = await WorkspacesService.getAllData(id);
      if(!workspaceData){
        res.status(404).send('Workspace not found');
      }else {
        res.status(200).json({
          success: true,
          data: workspaceData,
        });
      }

    } catch (error) {
      res.status(500).json('Internal server error');
    }
  }
}
