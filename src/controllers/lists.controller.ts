import { Request, Response } from 'express';
import {ListsDTO} from '../dtos/lists.dto';
import {validateError} from '../utils/validate-error.middleware';
import {ListsService} from '../services/lists.service';


export class ListsController {
  static async create(req: Request, res: Response) {
    try {
      const createListsDto = new ListsDTO(req.body);
      const errors = await validateError(createListsDto);
      if (errors.length) {
        res.status(400).json({
          success: false,
          errors,
        });
        return;
      }


      await ListsService.create(createListsDto);

      res.status(200).send('List created');
      return;
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  }
}