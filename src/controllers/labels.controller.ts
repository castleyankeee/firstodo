import { validateError } from '../utils/validate-error.middleware';
import { Request, Response } from 'express';
import {LabelsService} from '../services/labels.service';
import {LabelsDTO} from '../dtos/labels.dto';

export class LabelsController {
  static async newLabel(req: Request, res: Response) {
    try {
      const createNewLabelDto = new LabelsDTO(req.body);
      const errors = await validateError(createNewLabelDto);
      if (errors.length) {
        res.status(400).json({
          success: false,
          errors,
        });
        return;
      }
      await LabelsService.createLabels(createNewLabelDto);
      res.status(201).send('Label was created');
      return;
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  }

  static async assignLabel(req: Request, res: Response) {
    const { labelId, cardId } = req.body;
    try {
      await LabelsService.assignLabel(labelId, cardId);
         res.status(200).send('Label assigned to card');
      return;
    } catch (error) {
      console.error(error);
      res.status(500).send('Internal server error');
    }
  }
}
