import {CardsDto } from '../dtos/cards.dto';
import { Request, Response} from 'express';
import {validateError} from '../utils/validate-error.middleware';
import {CardsService} from '../services/cards.service';
import {UserRequest} from '../interfaces/user.interface';

export class CardsController {
  static async createCard(req: UserRequest, res: Response) {
    try {
      const createCardsDto = new CardsDto(req.body);
      const errors = await validateError(createCardsDto);
      if (errors.length) {
        res.status(400).json({
          success: false,
          errors,
        });
        return;
      }
      await CardsService.createCard(req.user.id, createCardsDto);
      res.status(200).send('Cards created');
      return;
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  }

  static async getOneCard(req: Request, res: Response) {
    const { id } = req.params;
    try {
      const card = await CardsService.getOneCard(id);
      if(!card){
        res.status(404).send('Card not found');
        return;
      }
      res.status(200).send(card);
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  }

  static async changeList(req: Request, res: Response) {
        const {cardId, newListId} = req.body;
    try {
      await CardsService.changeList(cardId,newListId);
      res.status(200).send('Lists updated');
      return;
    } catch (error) {
      res.status(500).send('Internal server error');
    }
  }
}

