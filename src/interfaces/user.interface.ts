import { Request } from 'express';

interface User {
  id: string;
}

export interface UserRequest extends Request {
    user: User
}