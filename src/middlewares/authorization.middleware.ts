const jwt = require('jsonwebtoken');
import * as process from 'process';
import {AppDataSource} from '../../data-source';
import {UsersEntity} from '../entities/users.entity';

const userRepository= AppDataSource.getRepository(UsersEntity);
export const authToken = async(req, res, next) => {
  const token = req.headers['authorization'];
  if (!token) {
    return res.status(401).send('Authorization failed. No access token.');
  }
  const decodedToken = jwt.verify(token.split(' ')[1], process.env.TOKEN_KEY);

  req.user = await userRepository.findOneBy({ id: decodedToken.id });

  next();
};




