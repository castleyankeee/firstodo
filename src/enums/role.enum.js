"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.RoleEnum = void 0;
var RoleEnum;
(function (RoleEnum) {
    RoleEnum["ADMIN"] = "admin";
    RoleEnum["DEVELOPER"] = "developer";
})(RoleEnum || (exports.RoleEnum = RoleEnum = {}));
