"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.ValabilityEnum = void 0;
var VisibilityEnum;
(function (ValabilityEnum) {
    ValabilityEnum["PUBLIC"] = "public";
    ValabilityEnum["PRIVATE"] = "private";
})(VisibilityEnum || (exports.ValabilityEnum = VisibilityEnum = {}));
