import { Router } from 'express';
import auth from './auth.router';
import workspaces from './workspace.router';
import lists from './lists.router';
import cards from './cards.router';
import labels from './labels.router';
import comments from './comments.router';

const routes = Router();

routes.use('/auth', auth);
routes.use('/workspaces', workspaces);
routes.use('/lists', lists);
routes.use('/cards', cards);
routes.use('/labels', labels);
routes.use('/comments', comments);
export default routes;
