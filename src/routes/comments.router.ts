import {Router} from 'express';
import {CommentsController} from '../controllers/comments.controller';


const comments = Router();

comments.post('/',CommentsController.createComment);
comments.get('/',CommentsController.getAllComments);
comments.patch('/edit',CommentsController.editComment);
comments.delete('/:id',CommentsController.deleteComment);

export default comments;

