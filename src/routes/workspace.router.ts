import {Router} from 'express';
import {WorkspacesController} from '../controllers/workspaces.controller';
import {authToken} from '../middlewares/authorization.middleware';

const workspaces= Router();

workspaces.post('/',authToken,WorkspacesController.createWorkspace);
workspaces.get('/:id', authToken, WorkspacesController.getAllData);


export default workspaces;