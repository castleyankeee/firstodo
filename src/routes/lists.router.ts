import { Router } from 'express';
import {ListsController} from '../controllers/lists.controller';

const lists = Router();

lists.post('/',ListsController.create);

export default lists;
