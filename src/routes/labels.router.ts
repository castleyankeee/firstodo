import {Router} from 'express';
import {LabelsController} from '../controllers/labels.controller';

const labels = Router();

labels.post('/', LabelsController.newLabel);
labels.post('/assign', LabelsController.assignLabel);

export default labels;