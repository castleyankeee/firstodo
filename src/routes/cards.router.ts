import { Router } from 'express';
import {CardsController} from '../controllers/cards.controller';
import {authToken} from '../middlewares/authorization.middleware';

const cards = Router();

cards.post('/',authToken,CardsController.createCard);
cards.get('/:id',CardsController.getOneCard);
cards.patch('/move',CardsController.changeList);

export default cards;