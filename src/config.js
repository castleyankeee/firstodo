"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.database = exports.main = void 0;
var dotenv_1 = require("dotenv");
var process_1 = require("process");
dotenv_1.default.config();
var main = {
    port: process_1.default.env.PORT,
};
exports.main = main;
var database = {
    host: process_1.default.env.TD_HOST,
    port: 5432,
    database: process_1.default.env.TD_DATABASE,
    user: process_1.default.env.TD_USER,
    password: process_1.default.env.TD_PASSWORD,
};
exports.database = database;
